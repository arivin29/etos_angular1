import { Vessel } from './../../_models/vessel';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class VesselsService {
    PHP_API_SERVER = "http://127.0.0.1";
    constructor(
        private http: HttpClient
    ) { }

    getVessel(): Observable<Vessel[]> {
        return this.http.get<Vessel[]>(`${this.PHP_API_SERVER}/api/vessels`);
    }
}
