import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AssetsRoutingModule } from './assets-routing.module';
import { WarehouseComponent } from './warehouse/warehouse.component';
import { MenuComponent } from './warehouse/menu/menu.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';

@NgModule({
    declarations: [WarehouseComponent, MenuComponent],
    imports: [
        CommonModule,
        AssetsRoutingModule,
        NgZorroAntdModule,
    ]
})
export class AssetsModule { }
