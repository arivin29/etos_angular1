export class Vessel {
  mid: string;
  vesselname: string;
  sipinumber: string;
  sipidate?: Date;
  sipiexpired?: Date;
  loadport: string;
  unloadport: string[];
  fishinggear: string;
  fma: string[];
  fishingarea: string[];
  capacity: number;
  owner: string;
  id: string;
}
