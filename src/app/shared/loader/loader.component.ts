import { TemplateService } from './../../tool/template.service';
import { Subject } from 'rxjs';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-loader',
    templateUrl: './loader.component.html'
})
export class LoaderComponent implements OnInit {

    constructor(
        private templateService: TemplateService
    ) {
        this.templateService.getLoading().subscribe(
            data => {
                this.isLoading = data;
            }
        );
    }

    isLoading: boolean = false;

    ngOnInit(): void {

    }
}
