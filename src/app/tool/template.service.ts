import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class TemplateService {

    constructor() { }
    isCollapsed = new BehaviorSubject<boolean>(true);
    // collspan: Observable<boolean> = this.isCollapsed.asObservable();

    setCollspan(value: boolean) {
        this.isCollapsed.next(value);
        console.log(value);
    }
    getCollspan()
    {
        return this.isCollapsed.asObservable();
    }

    /*-------------------------------------- Loader ----------------------------------------*/
    isLoading = new BehaviorSubject<boolean>(false);
    setLoading(status: boolean) {
        this.isLoading.next(status);
    }

    getLoading() {
        return this.isLoading.asObservable();
    }
    /*-------------------------------------- Loader ----------------------------------------*/

    isShowLeftMenu = new BehaviorSubject<boolean>(true);
    setShowLeftMenu(status: boolean) {
        this.isShowLeftMenu.next(status);
    }

    getShowLeftMenu() {
        return this.isShowLeftMenu.asObservable();
    }
    /*-------------------------------------- Loader ----------------------------------------*/
}
