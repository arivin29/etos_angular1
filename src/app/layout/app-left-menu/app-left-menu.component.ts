import { TemplateService } from './../../tool/template.service';

import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-app-left-menu',
    templateUrl: './app-left-menu.component.html'
})
export class AppLeftMenuComponent implements OnInit{
    isCollapsed : boolean = false;

    constructor(
        private templateService: TemplateService
    )
    {

    }

    ngOnInit() {
        this.templateService.getShowLeftMenu().subscribe(
            data => {
                this.isCollapsed = data;
            }
        )

    }
}
