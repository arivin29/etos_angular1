import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { AppLeftMenuComponent } from './app-left-menu.component';

describe('AppLeftMenuComponent', () => {
  let component: AppLeftMenuComponent;
  let fixture: ComponentFixture<AppLeftMenuComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AppLeftMenuComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AppLeftMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
