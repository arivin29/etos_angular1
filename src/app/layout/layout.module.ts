import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppTopMenuComponent } from './app-top-menu/app-top-menu.component';
import { AppLeftMenuComponent } from './app-left-menu/app-left-menu.component';
import { AuthComponent } from './auth/auth.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { AppComponent } from './app/app.component';
import { Demo1Component } from './app-top-menu/notification/demo1/demo1.component';

@NgModule({
  declarations: [AppTopMenuComponent, AppLeftMenuComponent, AuthComponent, AppComponent, Demo1Component],
  imports: [
    CommonModule,
    NgZorroAntdModule,
    RouterModule,
  ],
  providers : []
})
export class LayoutModule { }
