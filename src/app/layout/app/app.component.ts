import { Component, TemplateRef, ViewChild, OnInit } from '@angular/core';
import { TemplateService } from 'src/app/tool/template.service';

@Component({
    selector: 'app-app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    isCollapsed = false;
    isReverseArrow = false;
    width = 200;


    constructor(
        private templateService: TemplateService
    )
    {}

    ngOnInit(): void {
        this.templateService.getShowLeftMenu().subscribe(
            data=>{
                this.isCollapsed = data;
            }
        )
    }

    changeMenu()
    {
        console.log(this.isCollapsed);
        this.templateService.setShowLeftMenu(this.isCollapsed);
    }

}
